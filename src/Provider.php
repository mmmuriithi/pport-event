<?php namespace pPort\Services\Event;
use pPort;
class Provider extends pPort\Provider {

    public $alias='Event';

    public function register()
    {
    	 return "\\pPort\\Services\\Event\\EventService";
    }
}