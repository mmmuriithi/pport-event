<?php namespace pPort\Services\Event;
use pPort;
class EventService extends pPort\Service {

   static $disabled=array();

	public static $events = array();

	public function test()
	{
		echo 'Helo';
	}

	public static function register($event, $callback, $obj = null) {
		if (isset(self::$events[$event]))
		{
			self::$events[$event] = array();
		}
		if(is_string($callback))
		{
			if($obj===null)
			{
				if(class_exists($callback))
				{
					self::$events[$event][]=array($callback,'listen');
				}
				else
				{
					self::$events[$event][]=$callback;
				}
			}
			else
			{
				self::$events[$event][]=array($obj, $callback);
			}
		}
		else{
			self::$events[$event][]=$callback;
		}

	}
	



	public static function fire($event,$vars) {

		is_array($vars)?extract($vars):$vars;
		$config_hooks=Config::hooks();
		if(array_key_exists($event,$config_hooks))
		{
			return $config_hooks[$event]($vars);
		}
		elseif(isset(static::$events[$event]))
		{
			foreach (self::$events[$event] as $callback) {
				if(is_closure($callback))
				{
					return $callback($vars);
				}
				elseif(is_array($callback))
				{
					return call_user_func_array($callback,array($vars));
				}
				else
				{
					return call_user_func($callback);
				}
			}
		}
	}

	public static function __callStatic($func,$args)
	{
		$vars=!empty($args)?$args[0]:array();
		static::fire($func,$vars);
	}
}
